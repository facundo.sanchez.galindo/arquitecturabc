import { ethers } from "hardhat";

async function main() {
  const Faucet = await ethers.getContractFactory("Faucet_ArqBC")
  const faucet = await Faucet.deploy("0xa3fC68dCe6CF25b4fD8DfdB7245467cC5E13DC18")
  await faucet.deployed()

  console.log("Faucet deployado en ", faucet.address)
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
